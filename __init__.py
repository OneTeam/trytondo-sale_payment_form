# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import sale  
from . import statement
__all__ = ['register']


def register():
    Pool.register(
            sale.SalePaymentForm,
            statement.Line,
            module='sale_payment_form', type_='model')
    Pool.register(
            sale.WizardSalePayment,
            sale.WizardSaleReconcile,
            module='sale_payment_form', type_='wizard')
    Pool.register(
            module='sale_payment_form', type_='report')
